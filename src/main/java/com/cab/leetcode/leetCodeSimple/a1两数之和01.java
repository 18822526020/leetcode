package com.cab.leetcode.leetCodeSimple;

import java.util.HashMap;
import java.util.Map;

/*    给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的 两个 整数。

    你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。

    示例:

    给定 nums = [2, 7, 11, 15], target = 9

    因为 nums[0] + nums[1] = 2 + 7 = 9
    所以返回 [0, 1]
*/
public class a1两数之和01 {

    public static int[] twoSum(int[] nums, int target) {

        Map<Integer, Integer> map = new HashMap<>();
        int[] ints = new int[2];

        for (int i = 0; i < nums.length; i++) {
            Integer num = target - nums[i];
            if (map.containsKey(num)) {
                ints[0] = num;
                ints[1] = i;
                return ints;
            }
            map.put(nums[i], i);


        }
        throw new IllegalArgumentException("没有找到这两个数！");
    }
}

