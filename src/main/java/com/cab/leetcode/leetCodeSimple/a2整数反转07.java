package com.cab.leetcode.leetCodeSimple;

    // 给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。
    //
    // 示例 1:
    //
    // 输入: 123
    // 输出: 321
    // 示例 2:
    //
    // 输入: -123
    // 输出: -321
    // 示例 3:
    //
    // 输入: 120
    // 输出: 21
    // 注意:
    //
    // 假设我们的环境只能存储得下 32 位的有符号整数，则其数值范围为 [−2^31,  2^31 − 1]。请根据这个假设，
    // 如果反转后整数溢出那么就返回 0。
public class a2整数反转07 {

    public static int reverse(int x) {
        String x1 = x + "";
        char[] cha = x1.toCharArray();
        char[] chars = new char[cha.length];
        // 转换
        int length = chars.length;
        for (int i = 0; i < cha.length; i++) {
            if (cha[i] == '-') {
                chars[0] = '-';
                length++;
                continue;
            }
            chars[length -1- i] = cha[i];
        }
        String s = String.valueOf(chars);
        Long num = Long.parseLong(s);
        // 判断溢出
        if (num<-(1L<<31) || num >((1L<<31)-1 )) {
            return 0;
        }
        return num.intValue();
    }

    //官方解答
   // https://leetcode-cn.com/problems/reverse-integer/solution/
        public int reverse2(int x) {
            int rev = 0;
            while (x != 0) {
                int pop = x % 10;
                x /= 10;
                if (rev > Integer.MAX_VALUE/10 || (rev == Integer.MAX_VALUE / 10 && pop > 7)) return 0;
                if (rev < Integer.MIN_VALUE/10 || (rev == Integer.MIN_VALUE / 10 && pop < -8)) return 0;
                rev = rev * 10 + pop;
            }
            return rev;
        }


    public static void main(String[] args) {
        int reverse = reverse(-1111111119);
        System.out.println(reverse);
    }
}
