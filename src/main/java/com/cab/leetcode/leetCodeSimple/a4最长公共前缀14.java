package com.cab.leetcode.leetCodeSimple;

import sun.applet.Main;

/**
 * @program: leetcode
 * @description: $//{description}
 * @author: cab-liu
 * @create: 2018-12-03 20:51
 **/
/*
编写一个函数来查找字符串数组中的最长公共前缀。

如果不存在公共前缀，返回空字符串 ""。

示例 1:

输入: ["flower","flow","flight"]
输出: "fl"
示例 2:

输入: ["dog","racecar","car"]
输出: ""
解释: 输入不存在公共前缀。*/
public class a4最长公共前缀14 {
    public static  String longestCommonPrefix(String[] strs) {

        StringBuffer x = new StringBuffer("");
        try {
            for (int i = 0; i < strs[0].length(); i++) {

                char index = strs[0].charAt(i);

                for (String str : strs) {
                    if (index != str.charAt(i)) {
                        return x.toString();
                    }

                }
                x.append(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return x.toString();
    }

    public static void main(String[] args) {
        String[] strs = {"fsadfdfsd", "sdfsdfwf", "sdfsdf"};
        System.out.println(longestCommonPrefix(strs));
    }
}






















